#!/bin/bash

# If no suitable env, exit directly
if [ -z $CONTENTS_SOURCE_URL ]; then
  exit 1
fi

# Get content from git
git clone $CONTENTS_SOURCE_URL /data

# SIGTERM
save() {
  exit 0
}
trap save TERM

# check git and get content every 60 sec
cd /data
while true
do
  date
  sleep 60
  git pull
done