FROM alpine:latest

WORKDIR /

RUN apk update && \
    apk upgrade && \
    apk add --no-cache \
    bash \
    git

COPY ./contents-cloner.sh /contents-cloner.sh
RUN chmod a+x /contents-cloner.sh

CMD ["/contents-cloner.sh"]